#include <string.h>
#include "partitions/partition.h"
#include "hfs/hfs.h"

int main(int argc, char **argv) {
    if (strcmp(argv[1], "parts") == 0) {
        partition parts[50];
        uint64_t size = load_partitions(parts, 50);
        printf("MAJ:MIN| Size(Mb) Name\n");
        for (int i = 0; i < size; ++i) {
            printf("%3d:%3d| %8lu %s\n", parts[i].major, parts[i].minor, parts[i].size_mb, parts[i].name);
        }
    } else {
        hfsplus *fs = read_hfsplus(argv[1]);
        if (ERRNO == NO_RESERVED_SPACE) {
            puts("File is not an hfsplus filesystem: no reserved space");
            return -1;
        }
        if (ERRNO == BAD_ROOT_STRUCTURE) {
            puts("file system is broken");
            return -1;
        }
        puts(fs->pwd);
        char line[200];
        fgets(line, 200, stdin);
        uint32_t read = strlen(line);
        line[read - 1] = '\0';
        char *command = strtok(line, " ");
        char *buffer[100];
        while (strcmp(command, "exit") != 0) {
            if (strcmp(command, "ls") == 0) {
                uint32_t file_count = ls(fs, (char **) buffer);
                for (int i = 0; i < file_count; ++i) {
                    puts(buffer[i]);
                }
            } else if (strcmp(command, "cd") == 0) {
                char *pwd = cd(fs, strtok(NULL, " "));
                if (pwd == NULL) {
                    if (ERRNO == NO_SUCH_FILE_OR_DIR) {
                        puts("No such file or directory");
                    } else if (ERRNO == IS_FILE) {
                        puts("It is a file");
                    }
                } else {
                    puts(pwd);
                }
            } else if (strcmp(command, "pwd") == 0) {
                puts(pwd(fs));
            } else if (strcmp(command, "back") == 0) {
                char *pwd = back(fs);
                if (pwd == NULL && ERRNO == ALREADY_ON_TOP) {
                    puts("It is root");
                } else {
                    puts(pwd);
                }
            } else if (strcmp(command, "copy") == 0) {
                char *filename = strtok(NULL, " ");
                char *destination = strtok(NULL, " ");
                uint32_t res = copy(filename, destination, fs);
                if (res < 0) {
                    puts("Something went wrong");
                }
            } else {
                puts("Wrong command");
            }
            fgets(line, 200, stdin);
            read = strlen(line);
            line[read - 1] = '\0';
            command = strtok(line, " ");
        }
    }

}