#include "partition.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#define PARTITIONS_PATH "/proc/partitions"
#define READ_ONLY "r"
#define DELIMITERS " \n"

partition *parse_partition_string(char *source) {
    partition *result = (partition *) malloc(sizeof(partition));
    result->major = strtoll(strtok(source, DELIMITERS), NULL, 10);
    result->minor = strtoll(strtok(NULL, DELIMITERS), NULL, 10);
    result->blocks = strtoll(strtok(NULL, DELIMITERS), NULL, 10);
    result->size_mb = result->blocks / 1024;
    char *first = strtok(NULL, DELIMITERS);
    result->name = malloc(strlen(first) * sizeof(char));
    strcpy(result->name, first);
    return result;
}

uint64_t load_partitions(partition *buffer, uint64_t max_length) {

    FILE *partitions_file = fopen(PARTITIONS_PATH, READ_ONLY);
    char string[100];
    fgets(string, sizeof(string), partitions_file);
    fgets(string, sizeof(string), partitions_file);
    char *ptr = fgets(string, sizeof(string), partitions_file);
    uint64_t loaded = 0;

    while (ptr != NULL && loaded < max_length) {
        buffer[loaded++] = *parse_partition_string(string);
        ptr = fgets(string, sizeof(string), partitions_file);
    }
    fclose(partitions_file);
    return loaded;
}

void free_partition(partition *ptr, uint64_t partitions_size) {
    for (int i = 0; i < partitions_size; ++i) {
        free(ptr[i].name);
    }
    free(ptr);
}