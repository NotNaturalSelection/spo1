#ifndef SOFTWARE1LIB_HFS_H
#define SOFTWARE1LIB_HFS_H

#include <stdbool.h>
#include "btree.h"

typedef enum {
    NO_SUCH_FILE_OR_DIR,
    IS_FILE,
    ALREADY_ON_TOP,
    NO_RESERVED_SPACE,
    BAD_ROOT_STRUCTURE
} ERROR;

ERROR ERRNO;

typedef struct {
    uint32_t startBlock;
    uint32_t blockCount;
} extent_descriptor;

typedef struct {
    uint64_t logicalSize;
    uint32_t clumpSize;
    uint32_t totalBlocks;
    extent_descriptor extents[8];
} fork_data;

typedef struct {
    uint16_t signature;
    uint16_t version;

    uint32_t attributes;
    uint32_t lastMountedVersion;
    uint32_t journalInfoBlock;
    uint32_t createDate;
    uint32_t modifyDate;
    uint32_t backupDate;
    uint32_t checkedDate;
    uint32_t fileCount;
    uint32_t folderCount;
    uint32_t blockSize;
    uint32_t totalBlocks;
    uint32_t freeBlocks;
    uint32_t nextAllocation;
    uint32_t rsrcClumpSize;
    uint32_t dataClumpSize;

    uint32_t nextCatalogID;

    uint32_t writeCount;
    uint64_t encodingsBitmap;
    uint32_t finderInfo[8];

    fork_data *allocationFile;
    fork_data *extentsFile;
    fork_data *catalogFile;
    fork_data *attributesFile;
    fork_data *startupFile;
} volume_header;

typedef struct {
    int16_t recordType;
    uint16_t flags;
    uint32_t valence;
    uint32_t folderID;
    uint32_t createDate;
    uint32_t contentModDate;
    uint32_t attributeModDate;
    uint32_t accessDate;
    uint32_t backupDate;
    uint32_t textEncoding;
    uint32_t reserved2;
} catalog_folder;

typedef struct {
    int16_t recordType;
    uint16_t flags;
    uint32_t reserved1;
    uint32_t fileID;
    uint32_t createDate;
    uint32_t contentModDate;
    uint32_t attributeModDate;
    uint32_t accessDate;
    uint32_t backupDate;
    uint32_t textEncoding;
    uint32_t reserved2;

    fork_data dataFork;
    fork_data resourceFork;
} catalog_file;

typedef struct {
    char *pwd;
    uint32_t pathFolderIds[500];
    uint32_t path_folder_depth;
    uint32_t current_folder_id;
    catalog_folder *current_folder_info;
    volume_header *vol_header;
    btree *catalog_file;
} hfsplus;

hfsplus *read_hfsplus(char *filename);

char *pwd(hfsplus *fs);

uint32_t ls(hfsplus *fs, char **buffer);

char *cd(hfsplus *fs, char *destination);

char *back(hfsplus *fs);

int32_t copy_file(char *destination, hfsplus *fs, int32_t offset);

int32_t copy_dir(char *destination, hfsplus *fs, int32_t offset);

int32_t copy(char *name, char *destination, hfsplus *fs);

#endif //SOFTWARE1LIB_HFS_H
