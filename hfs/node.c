#include <stdio.h>
#include <unistd.h>
#include "node.h"
#include "../be_read.h"

node_descriptor *create_node_descriptor(uint64_t fd, uint32_t offset) {
    lseek(fd, offset, SEEK_SET);
    node_descriptor *descriptor = malloc(sizeof(node_descriptor));
    read_uint32(&descriptor->fLink, fd);
    read_uint32(&descriptor->bLink, fd);
    read_int8(&descriptor->kind, fd);
    read_uint8(&descriptor->height, fd);
    read_uint16(&descriptor->numRecords, fd);
    read_uint16(&descriptor->reserved, fd);
    return descriptor;
}

void fill_record_pointers(data_node *ptr, uint64_t fd, uint32_t offset) {
    int32_t arr_size = ptr->descriptor->numRecords + 1;
    ptr->record_pointers = malloc(SIZE16 * arr_size);
    lseek(fd, offset - (arr_size * SIZE16), SEEK_SET);
    for (int i = arr_size - 1; i >= 0; --i) {
        read_uint16(&ptr->record_pointers[i], fd);
    }
}

void fill_records(data_node *ptr, uint64_t fd, uint32_t offset) {
    ptr->records = malloc(sizeof(char *) * ptr->descriptor->numRecords);
    for (int i = 0; i < ptr->descriptor->numRecords; ++i) {
        size_t record_size = ptr->record_pointers[i + 1] - ptr->record_pointers[i];
        ptr->records[i] = malloc(record_size);
        lseek(fd, ptr->record_pointers[i] + offset, SEEK_SET);
        read(fd, ptr->records[i], record_size);
    }
}

void free_node(data_node *ptr) {
    if (ptr != NULL) {
        if (ptr->descriptor != NULL){
            free(ptr->descriptor);
        }
        free(ptr);
    }
//    free(ptr->records);
//    free(ptr->record_pointers);

}