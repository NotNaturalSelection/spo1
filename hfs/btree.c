#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "btree.h"
#include "node.h"
#include "header_node.h"
#include "../be_read.h"

#define KEY_SIZE_LENGTH 2

data_node *read_node(btree *btree_ptr, bool with_data, uint32_t node_size, uint32_t node_number) {
    uint32_t node_offset = btree_ptr->btree_offset + node_size * node_number;
    data_node *result = malloc(sizeof(data_node));
    result->descriptor = create_node_descriptor(btree_ptr->fd, node_offset);
    fill_record_pointers(result, btree_ptr->fd, node_offset + node_size);
    if (with_data) {
        fill_records(result, btree_ptr->fd, node_offset);
    }
    return result;
}

data_node *get_node(uint32_t node_number, btree *btree_ptr, bool with_data) {
    if (btree_ptr->leaf_nodes[node_number - btree_ptr->no_leaf_nodes_num] == NULL) {
        btree_ptr->leaf_nodes[node_number - btree_ptr->no_leaf_nodes_num] = read_node(
                btree_ptr,
                with_data,
                get_header_record(btree_ptr)->nodeSize,
                node_number
        );
    }
    return btree_ptr->leaf_nodes[node_number - btree_ptr->no_leaf_nodes_num];
}

catalog_key *get_catalog_key(data_node *node_ptr, uint32_t record_number, btree *btree_ptr, uint32_t node_index) {
    catalog_key *res = malloc(sizeof(catalog_key));

    uint32_t offset = btree_ptr->btree_offset + node_index * get_header_record(btree_ptr)->nodeSize +
                      node_ptr->record_pointers[record_number];
    lseek(btree_ptr->fd, offset, SEEK_SET);
    read_uint16(&res->key_length, btree_ptr->fd);
    read_uint32(&res->parent_id, btree_ptr->fd);
    read_uint16(&res->sym_length, btree_ptr->fd);
    res->node_name = malloc(res->sym_length);
    for (int i = 0; i < res->sym_length; ++i) {
        read_char(&res->node_name[i], btree_ptr->fd);
        read_char(&res->node_name[i], btree_ptr->fd);
    }

    return res;
}


int32_t get_value_offset_by_key(char *key_to_comp, btree *btree_ptr, uint32_t parent_id, int16_t *type) {
    int32_t index = get_header_record(btree_ptr)->firstLeafNode;
    data_node *current;

    while (index != 0) {
        current = get_node(index, btree_ptr, true);
        for (int i = 0; i < current->descriptor->numRecords; ++i) {
            catalog_key *key = get_catalog_key(current, i, btree_ptr, index);
            if (strlen(key_to_comp) == key->sym_length && memcmp(key_to_comp, key->node_name, key->sym_length) == 0 && key->parent_id == parent_id) {
                uint32_t offset = btree_ptr->btree_offset + index * get_header_record(btree_ptr)->nodeSize +
                                  current->record_pointers[i] + KEY_SIZE_LENGTH + key->key_length;
                lseek(btree_ptr->fd, offset, SEEK_SET);

                read_int16(type, btree_ptr->fd);
                if (*type == FileRecord || *type == FolderRecord){
                    free_key(key);
                    return offset;
                }
            }
            free_key(key);
        }
        index = current->descriptor->fLink;
    }
    return -1;
}

uint32_t set_value_ptr_by_id(btree *btree_ptr, uint32_t item_id, catalog_key *key_to_fill) {
    uint32_t index = get_header_record(btree_ptr)->firstLeafNode;
    data_node *current;
    while (index != 0) {
        current = get_node(index, btree_ptr, true);
        catalog_key *key;
        for (int j = 0; j < current->descriptor->numRecords; ++j) {
            key = get_catalog_key(current, j, btree_ptr, index);
            if (key->parent_id == item_id) {
                key_to_fill->sym_length = key->sym_length;
                key_to_fill->key_length = key->key_length;
                key_to_fill->parent_id = key->parent_id;
                key_to_fill->node_name = key->node_name;

                uint32_t offset = btree_ptr->btree_offset + index * get_header_record(btree_ptr)->nodeSize +
                                  current->record_pointers[j] + key_to_fill->key_length + KEY_SIZE_LENGTH;
                lseek(btree_ptr->fd, offset, SEEK_SET);
                uint32_t result = current->record_pointers[j + 1] - current->record_pointers[j];
                return result;
            }
            free_key(key);
        }
        index = current->descriptor->fLink;
    }
    return -1;
}

uint32_t get_children_of_id(btree *btree_ptr, uint32_t item_id, char **buffer) {
    uint32_t index = get_header_record(btree_ptr)->firstLeafNode;

    data_node *current;
    uint32_t result = 0;
    while (index != 0) {
        current = get_node(index, btree_ptr, true);
        catalog_key *key;
        for (int j = 0; j < current->descriptor->numRecords; ++j) {
            key = get_catalog_key(current, j, btree_ptr, index);
            int16_t type;
            read_int16(&type, btree_ptr->fd);

            if (key->parent_id == item_id && (type == FolderRecord || type == FileRecord) &&
                strcmp(key->node_name, "") != 0) {
                buffer[result++] = key->node_name;
                free(key);//because key.node_name is put into buffer
            } else {
                free_key(key);
            }
        }
        index = current->descriptor->fLink;
    }
    return result;
}

btree *init_btree(uint64_t fd, uint32_t btree_offset) {
    btree *result = malloc(sizeof(btree));
    result->btree_offset = btree_offset;
    result->fd = fd;

    lseek(fd, btree_offset, SEEK_SET);
    result->header_node = create_header_node(result);
    header_record *h_record = get_header_record(result);
    result->leaf_nodes_num = h_record->lastLeafNode -
                             h_record->firstLeafNode + 1;
    result->no_leaf_nodes_num = h_record->totalNodes - h_record->freeNodes - result->leaf_nodes_num;
    result->leaf_nodes = malloc(sizeof(void *) * result->leaf_nodes_num);
    if (result->leaf_nodes_num == 1) {
        result->leaf_nodes[0] = get_node(h_record->firstLeafNode, result, true);
    }
    return result;
}

void free_key(catalog_key *ptr) {
    free(ptr->node_name);
    free(ptr);
}